server {
    listen  80;
    listen  [::]:80;

    client_max_body_size 128M;
    charset utf-8;

    server_name  main-dev.loc;

    set $host_path /var/www/app/web/;

    access_log  /var/log/nginx/docker.access.log;
    error_log   /var/log/nginx/docker.error.log;

    location / {
        root   $host_path;
        index  index.php index.html index.htm;

        try_files $uri $uri/ /index.php?$args;
    }

    #error_page  404    /404.html;

    # redirect server error pages to the static page /50x.html
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # deny accessing php files for the /assets directory
    location ~ ^/assets/.*\.php$ {
        deny all;
    }

    location ~ \.php$ {
        access_log off;
        root $host_path;
        include fastcgi_params;
        proxy_read_timeout 120s;
        fastcgi_send_timeout 180s;
        fastcgi_read_timeout 180s;
        proxy_buffers 8 128k;
        proxy_buffer_size 128k;
        proxy_busy_buffers_size   128k;
        fastcgi_pass php:9100;
        fastcgi_index  index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;

        try_files $uri $uri/ =404;
    }

    location ~* /\. {
        deny all;
    }
}